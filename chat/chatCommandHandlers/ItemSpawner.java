package chat.chatCommandHandlers;

import item.Item;
import Player.PlayerConnection;
import Player.Character;
import Tools.StringTools;
import Connections.Connection;
import chat.ChatCommandExecutor;

public class ItemSpawner implements ChatCommandExecutor {


	public void execute(String[] parameters, Connection source) {
		System.out.println("Received chat command to spawn an item!");
		for(int i=0;i<parameters.length;i++) {
			System.out.println("Command param[" + (i+1) + "] : " + parameters[i]);
			if(StringTools.isInteger(parameters[0]) && parameters[i].length() == 9) {
				System.out.println("Param[" + (i+1) + "] is of integer type length 9!");
				int itemID = Integer.parseInt(parameters[0]);
				Character cur = ((PlayerConnection)source).getActiveCharacter();
				System.out.println("About to spawn item: " + itemID + " at coordinates: " + cur.getlastknownX() + "," + cur.getlastknownY() );
				
				source.addWrite(Item.itemSpawnPacket(cur.getCharID(), itemID, cur.getlastknownX(), cur.getlastknownY()));
			}
		}
	}

}
