package ServerCore;

import java.util.Iterator;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.LinkedBlockingQueue;

import Connections.Connection;

public class ConnectionFinalizer implements Runnable {

	private CyclicBarrier barrier = new CyclicBarrier(3);
	private LinkedBlockingQueue<Connection> finaList = new LinkedBlockingQueue<Connection>();
	private ServerFacade sf = ServerFacade.getInstance();
	
	@Override
	public void run() {
			if(!this.finaList.isEmpty()) {
				//wait until both read and write selectors are done with their cycle
				while(this.barrier.getNumberWaiting() != 2) {
					try { Thread.sleep(10); } catch (InterruptedException e) {	}
				}
				//disconnect connections waiting to be finalized
				Iterator<Connection> cider = this.finaList.iterator();
				Connection tmp = null;
				while(cider.hasNext()) {
					tmp = cider.next();
						
					this.sf.removeConnection(tmp.getChan());
					tmp.unregister();
					tmp.disconnect();
						
					cider.remove();
				}
				//allow threads to continue by having third and final thread reach the barrier
				try {
					this.barrier.await();			
				} catch (InterruptedException e) {
					e.printStackTrace();
					this.barrier.reset();
				} catch (BrokenBarrierException e) {
					e.printStackTrace();
					Thread.currentThread().interrupt();
					this.barrier.reset();
				}
			}
	}
	
	public CyclicBarrier getBarrier() {
		return this.barrier;
	}

	public void addFinalizableConnection(Connection con) {
		if(con.getChan().isOpen()) {
			this.finaList.offer(con);
		}
	}
	
	public boolean isWaitingFinalization() {
		return (this.finaList.isEmpty()) ? false : true;
	}
	
	public LinkedBlockingQueue<Connection> getFinalizeList() {
		return this.finaList;
	}
}
