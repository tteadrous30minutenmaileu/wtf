package ServerCore;

import java.io.IOException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import Queue.ReadHandler;
import ThreadPool.PacketHandlerPool;

public class ReadSelectorThread implements Runnable {

	private boolean running = true;
	private Selector select;
	private PacketHandlerPool ppool = PacketHandlerPool.getInstance();
	private ConnectionFinalizer disconnector;
	
	public ReadSelectorThread(Selector se, ConnectionFinalizer cf) {
		this.select = se;
		this.disconnector = cf;
	}
	
	public void run() {
		int selected = 0;
		while(this.running) {
			if(this.disconnector.isWaitingFinalization()) {
				try {
					try {
						this.disconnector.getBarrier().await(2000, TimeUnit.MILLISECONDS);
					} catch (TimeoutException e) {
						// TODO implement exception strategy for when max time reached
						this.disconnector.getFinalizeList().clear();
						this.disconnector.getBarrier().reset();
					}
				} catch (InterruptedException e) {
					this.disconnector.getFinalizeList().clear();
					this.disconnector.getBarrier().reset();
				} catch (BrokenBarrierException e) {
					this.disconnector.getFinalizeList().clear();
					this.disconnector.getBarrier().reset();
				}
			}
			try {				
				selected = this.select.select(30);
			} catch (IOException e) {
				e.printStackTrace();
			}
			if(selected > 0) {
				Set<SelectionKey> readySet = this.select.selectedKeys();
				CountDownLatch counter = new CountDownLatch(readySet.size());
			
				Iterator<SelectionKey> keyIter = readySet.iterator();
				SelectionKey curKey = null;
			
				while(keyIter.hasNext()) {
					curKey = keyIter.next();
					if(curKey.isValid() && curKey.isReadable()) {
							this.ppool.executeProcess(new ReadHandler((SocketChannel)curKey.channel(), counter));
					} else if(!curKey.isValid()) {
						curKey.cancel();
						counter.countDown();
					} else {
						counter.countDown();
					}
					keyIter.remove();
				}
				try {
					counter.await(1000, TimeUnit.MILLISECONDS);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			} else {
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public void setRunning(boolean bol) {
		this.running = bol;
	}
	
	public Selector getSelector() {
		return this.select;
	}
	
}
