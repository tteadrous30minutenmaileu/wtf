package ServerCore;

import java.io.IOException;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.LinkedBlockingQueue;

import ThreadPool.PacketHandlerPool;

import Connections.Connection;

public class SelectorPair {
	
	private Selector readSelector;
	private Selector writeSelector;
	private ConnectionFinalizer disconnector;
	private PacketHandlerPool threadPool = PacketHandlerPool.getInstance();
	private Thread reader;
	private Thread writer;
	
	public SelectorPair() {
		try {
			this.readSelector = Selector.open();
			this.writeSelector = Selector.open();
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.disconnector = new ConnectionFinalizer();
		this.reader = new Thread(new ReadSelectorThread(this.readSelector, this.disconnector));
		this.writer = new Thread(new WriteSelectorThread(this.writeSelector, this.disconnector));
		this.reader.setPriority(Thread.MAX_PRIORITY);
		this.writer.setPriority(Thread.MAX_PRIORITY);
		this.reader.start();
		this.writer.start();
	}

	public void registerChannel(SocketChannel chan) {
			try {
				chan.register(this.readSelector, SelectionKey.OP_READ);
				chan.register(this.writeSelector, SelectionKey.OP_WRITE);
				chan.keyFor(this.writeSelector).interestOps(SelectionKey.OP_READ);
			} catch (ClosedChannelException e) {
				e.printStackTrace();
			}
	}

	public void unregisterChannel(SocketChannel chan) {
		chan.keyFor(this.readSelector).cancel();
		chan.keyFor(this.writeSelector).cancel();
	}
	
	public void flipRead(SocketChannel chan, int ops) {
		chan.keyFor(this.readSelector).interestOps(ops);
	}
	
	public void flipWrite(SocketChannel chan, int ops) {
		chan.keyFor(this.writeSelector).interestOps(ops);
	}
	
	public Selector getReadSelector() {
		return this.readSelector;
	}
	
	public Selector getWriteSelector() {
		return this.writeSelector;
	}
	
	public CyclicBarrier getBarrier() {
		return this.disconnector.getBarrier();
	}
	
	public void registerForFinalization(Connection con) {
		this.disconnector.addFinalizableConnection(con);
		this.threadPool.executeProcess(this.disconnector);
	}
	
	public boolean needSync() {
		return this.disconnector.isWaitingFinalization();
	}
	
	public LinkedBlockingQueue<Connection> getFinalizeList() {
		return this.disconnector.getFinalizeList();
	}
}
