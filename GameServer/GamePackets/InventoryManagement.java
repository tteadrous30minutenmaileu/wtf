package GameServer.GamePackets;

import java.nio.ByteBuffer;

import Connections.Connection;
import Encryption.Decryptor;
import Player.Character;
import Player.PlayerConnection;
import Tools.BitTools;

public class InventoryManagement implements Packet {

	@Override
	public void execute(ByteBuffer buff) {
		// TODO Auto-generated method stub
		
	}

	
	public ByteBuffer returnWritableByteBuffer(ByteBuffer buffyTheVampireSlayer, Connection con) {
		System.out.println("Handling inventory");
		byte[] decrypted = new byte[(buffyTheVampireSlayer.get(0) & 0xFF)-8];
		
		for(int i=0;i<decrypted.length;i++) {
			decrypted[i] = (byte)(buffyTheVampireSlayer.get(i+8) & 0xFF);
		}
		
		decrypted = Decryptor.Decrypt(decrypted);
		byte[] inv = new byte[28];
		Character cur = ((PlayerConnection)con).getActiveCharacter();
		
		inv[0] = (byte)inv.length;
		inv[4] = (byte)0x04;
		inv[6] = (byte)0x10;
		
		byte[] chid = BitTools.intToByteArray(cur.getCharID());
		
		for(int i=0;i<4;i++) {
			inv[12+i] = chid[i];
			inv[19+i] = decrypted[i+1];
		}
		
		inv[16] = (byte)0x01;
		inv[18] = decrypted[0];
		
		return ByteBuffer.wrap(inv);
	}

}
