package Player;


import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;

import timer.MoveSyncTimer;
import timer.SystemTimer;

import logging.ServerLogger;


import ServerCore.ServerFacade;
import Tools.BitTools;
import World.Grid;
import World.Location;
import World.WMap;
import World.Area;
import World.Waypoint;

public class Character implements Location {
	public static final int factionNeutral = 1;
	public static final int factionLawful = 2;
	public static final int factionEvil = 3;
	private String name = "NOP";
	private int level;
	private short[] stats;
	private int statPoints;
	private int skillPoints;
	private int characterClass;
	private Waypoint location;
	private int faction;
	private int attack, defence, maxhp, hp, mana, stamina;
	private int charID;
	private int currentMap;
	private int [] areaCoords;
	private Area area;
	private Grid grid;
	private byte[] characterDataPacket;
	private List<Integer> iniPackets = new ArrayList<Integer>();
	private ServerLogger log = ServerLogger.getInstance();
	private Player pl;
	private MoveSyncTimer timer;
	private int speed = 10;
	private int syncDistance = 30;
	private WMap wmap = WMap.getInstance();
	
	
	
	
	public Character(Player pl) {
		this.pl = pl;
		 this.location = new Waypoint(0,0);
	}

	public Character() {
		//TODO: durp durp
		 this.location = new Waypoint(0,0);
	}
	
	/*
	 * Handle all logic required when character is selected in selection screen
	 * and player enters the game
	 */
	public void joinGameWorld() {
		this.wmap.addCharacter(this);
		if (this.wmap.gridExist(currentMap)){
			this.grid = this.wmap.getGrid(this.currentMap);
			this.area = this.grid.update(this);
			this.area.addMember(this);
		}
		else {
			log.logMessage(Level.SEVERE, this, "Failed to load grid for character "+this.charID +" map:" +this.currentMap + ", disconnecting");
			ServerFacade.getInstance().finalizeConnection(this.GetChannel());
		}
	}
	
	/*
	 * Quite the opposite of joining the game world
	 */
	public void leaveGameWorld() {
		this.area.rmMember(this);
		this.wmap.rmCharacter(charID);
		this.iniPackets.clear();
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	
	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public short[] getStats() {
		return stats;
	}

	public void setStats(short[] stats) {
		this.stats = stats;
	}

	public int getStatPoints() {
		return statPoints;
	}

	public void setStatPoints(int statPoints) {
		this.statPoints = statPoints;
	}

	public int getSkillPoints() {
		return skillPoints;
	}

	public void setSkillPoints(int skillPoints) {
		this.skillPoints = skillPoints;
	}

	public int getCharacterClass() {
		return characterClass;
	}

	public void setCharacterClass(int characterClass) {
		this.characterClass = characterClass;
	}

	public int getFaction() {
		return faction;
	}

	public void setFaction(int faction) {
		this.faction = faction;
	}

	public int getAttack() {
		return attack;
	}

	public void setAttack(int attack) {
		this.attack = attack;
	}

	public int getDefence() {
		return defence;
	}

	public void setDefence(int defence) {
		this.defence = defence;
	}

	public int getHp() {
		return hp;
	}

	public void setHp(int hp) {
		this.hp = hp;
	}

	public int getMana() {
		return mana;
	}

	public void setMana(int mana) {
		this.mana = mana;
	}

	public int getStamina() {
		return stamina;
	}

	public void setStamina(int stamina) {
		this.stamina = stamina;
	}

	public Player getPlayer() {
		return pl;
	}

	public void setPlayer(Player pl) {
		this.pl = pl;
	}

	public Area getArea() {
		return area;
	}

	public Grid getGrid() {
		return grid;
	}

	public int[] getAreaCoords() {
		return areaCoords;
	}

	@Override
	public int getuid() {
		// TODO Auto-generated method stub
		return this.charID;
	}

	@Override
	public void setuid(int uid) {
		// TODO Auto-generated method stub
		this.charID = uid;		
	}

	@Override
	public float getlastknownX() {
		// TODO Auto-generated method stub
		return this.location.getX();
	}

	@Override
	public float getlastknownY() {
		// TODO Auto-generated method stub
		return this.location.getY();
	}
	@Override
	public SocketChannel GetChannel() {
		return this.pl.getSc();
	}

	@Override
	public short getState() {
		// TODO Auto-generated method stub
		return 0;
	}

	public int getCharID() {
		return charID;
	}

	public void setCharID(int charID) {
		this.charID = charID;
	}

	public void setX(float x) {
		this.location.setX(x);
	}

	public void setY(float y) {
		this.location.setY(y);
	}

	public int getMaxHp() {
		return maxhp;
	}

	public void setMaxHp(int max) {
		this.maxhp = max;
	}

	public int getCurrentMap() {
		return currentMap;
	}

	public void setCurrentMap(int currentMap) {
		this.currentMap = currentMap;
	}

	public byte[] getCharacterDataPacket() {
		return characterDataPacket;
	}

	public void setCharacterDataPacket(byte[] characterDataPacket) {
		this.characterDataPacket = characterDataPacket;
	}
	public void updateLocation(float x, float y){
		/*
		if (this.timer != null){ if (!this.timer.isCompleted()) this.timer.cancel(); }
		if (WMap.distance(x, y, this.getX(), this.getY()) > this.syncDistance){
			this.activesyncTest(this.getX(), this.getY(), x, y);
		}*/
		
		this.setX(x);
		this.setY(y);
		Area t = this.grid.update(this);
		if (t != null){
			if (t != this.area){
				this.area.moveTo(this, t);
				this.area = t;
				this.area.addMember(this);
			}
		}
	}
	// this method should only be called by active sync timer
	public void syncLocation(float x, float y){
		this.setX(x);
		this.setY(y);
		Area t = this.grid.update(this);
		if (t != null){
			if (t != this.area){
				this.area.moveTo(this, t);
				this.area = t;
				this.area.addMember(this);
			}
		}
	}
	
	@SuppressWarnings("unused")
	private void activesyncTest(float x, float y, float dx, float dy) {
		float deltax = WMap.distance(x, dx);
		float deltay = WMap.distance(y, dy);
		float hyp = WMap.distance(x, y, dx, dy);
		float sina = deltay / hyp;
		float cosa = deltax / hyp;
		float mpx = 1;
		float mpy = 1;
		float done = this.syncDistance;
		long period = (long)( (hyp / this.speed) * 1000);
		long delay = (long)((this.syncDistance / this.speed) * 1000);
		List<Waypoint> wplist = new ArrayList<Waypoint>();
		Waypoint wp = null;
		
		if (dx < x) mpx = -1;
		if (dy < y) mpy = -1;
		if (cosa == 1) cosa = 0;
		if (sina == 1) sina = 0;
		
		//System.out.println("Estimated total time to travel: " + period +"ms Interval time" +  delay + "ms");
		//System.out.println("Start coords x: " + x + " y: " +y);
		for (done = this.syncDistance; done < hyp; done += this.syncDistance){
			x += mpx * (cosa * this.syncDistance);
			y += mpy * (sina * this.syncDistance);
			wp = new Waypoint(x,y);
			wplist.add(wp);
			//System.out.println("inteval coords x: " + x + " y:" + y);
		}
		this.timer = new MoveSyncTimer(wplist, this);
		SystemTimer.getInstance().addTask(this.timer, delay, delay);
		//System.out.println("end coords x: " + dx + " y: " +dy);
	}

	/*
	 * Holy mother of a monster packet.
	 * Format a character data packet for this character based on it's attributes.
	 */
	public byte[] initCharPacket() {
        byte[] cdata = new byte[649];
        byte[] charname = this.getName().getBytes();
 
        for(int i=0;i<charname.length;i++) {
                cdata[i] = charname[i];
        }
        if(this.getCharacterClass() == 2) {
                cdata[40] = 0x02;       //gender byte(2 = female(yeah.. don't ask me why CRS add gender byte in their packet))
                cdata[48] = 0x02;       //sin class is 2
        } else {
            	cdata[40] = 0x01; //male(obviously duh)
                cdata[48] = (byte)this.characterClass; //class byte
        }
        cdata[54] = (byte)this.getLevel();
        
		//0 int, 1 agi, 2 vit, 3 dex, 4 str | int, vit, agi, str, dex
        for(int i=0;i<this.stats.length;i++) {
        	byte[] tmp = BitTools.shortToByteArray(this.stats[i]);
        	cdata[572+(i*2)] = tmp[0];
        	cdata[572+(i*2)+1] = tmp[1];
        }
        
        cdata[600] = (byte)this.statPoints;
                
        for(int i=0;i<16;i++) { cdata[i+17] = (byte)0x30; }
        
        cdata[34] = (byte)0x01;
        cdata[42] = (byte)0x01;
        cdata[44] = (byte)0x01; //standard its 01 but 00 gives no errors, no explanation yet
        cdata[52] = (byte)0x02; //standard its 02 but 00 goes as well nothing changes though, no explanation yet
                
        byte[] stuff = new byte[] { (byte)0x00, (byte)0xa0, (byte)0x01, (byte)0x00, (byte)0x00, (byte)0x7c, (byte)0x01, (byte)0x00,                               
                (byte)0x00, (byte)0x01, (byte)0x00, (byte)0x00, (byte)0x00, 
                (byte)0xf3, (byte)0xd4, (byte)0x13, (byte)0xc5, (byte)0x9a, 
                (byte)0xb9, (byte)0xbd, (byte)0xc5, (byte)0x00, (byte)0x00, 
                (byte)0x02, (byte)0x00 }; 
        
     
        for(int i=0;i<stuff.length;i++) {
                cdata[i+66-8-3] = stuff[i];    
        }
        
        this.setCharacterDataPacket(cdata);
        return cdata;
	}
	// send packet buf to all nearby players
	public void sendToMap(ByteBuffer buf) {
		synchronized(this.iniPackets) {
			Iterator<Integer> iter = this.iniPackets.iterator();
				while(iter.hasNext()) {
					Integer plUid = iter.next();               
					if (plUid != this.charID){
						Character ch = this.wmap.getCharacter(plUid.intValue());
						if(ch != null) {
							ServerFacade.getInstance().addWriteByChannel(ch.GetChannel(), buf);
						}
					}
				}
		}
		//this.area.sendToMembers(this.getuid(), buf);
	}
	
	// notify our area that current players has changed
	public void notifyEnvironment() {
		this.area.notifyMembers();
	}

	// receive updated list for nearby objects
	public void updateEnvironment(List<Integer> players) {
		//System.out.println("Character: " + this.charID + " has got player list of size: " + players.size());
		Iterator<Integer> plIter = this.iniPackets.iterator();
		Integer tmp = null;
		
		while(plIter.hasNext()) {
			tmp = plIter.next();
			if(!players.contains(tmp)) {//if character is no longer in range, remove it
				// this.incVanish(tmp)
				if (this.wmap.CharacterExists(tmp)){
					ServerFacade.getInstance().addWriteByChannel(this.GetChannel(), ByteBuffer.wrap(this.getVanishByID(tmp)));
				}
				plIter.remove();	
			}
			else { // remove from need ini list if we already have it on the list
				players.remove(tmp);
			}
			if(!this.wmap.CharacterExists(tmp)) { // remove if not a valid character
				players.remove(tmp);
			} 
		}
		
		this.sendInit(players);
		//this.sendVanish();
	}
	// send initial packets to players who don't already have ours
	private void sendInit(List<Integer> sendList) {
		Iterator<Integer> siter = sendList.iterator();
		Integer tmp = null;
		while(siter.hasNext()) {
			tmp = siter.next();
			if (this.wmap.CharacterExists(tmp) && tmp != this.getuid()){
				Character t = this.wmap.getCharacter(tmp);
				SocketChannel sc = t.GetChannel();
				ServerFacade.getInstance().getConnectionByChannel(sc).addWrite(ByteBuffer.wrap(this.extCharPacket()));
				// if (this.vanish.containsKey(tmp)) this.vanish.remove(tmp);
			}
		}
		this.iniPackets.addAll(sendList);
	}
	
	// return external character packet
	private byte[] extCharPacket() {
		byte[] cedata = new byte[612];
		short length = (short)cedata.length;
		byte[] lengthbytes = BitTools.shortToByteArray(length);
		byte[] chID = BitTools.intToByteArray(this.charID);
		byte[] chName = this.name.getBytes();
		byte[] xCoords = BitTools.floatToByteArray(this.getlastknownX());
		byte[] yCoords = BitTools.floatToByteArray(this.getlastknownY());
		
		cedata[0] = lengthbytes[0];
		cedata[1] = lengthbytes[1];
		cedata[4] = (byte)0x05;
		cedata[6] = (byte)0x01;
		cedata[8] = (byte)0x01;
		
		for(int i=0;i<4;i++) {
			cedata[i+12] = chID[i]; //character ID
			cedata[i+88] = xCoords[i]; //location x
			cedata[i+92] = yCoords[i]; //location y
		}
		
		for(int i=0;i<chName.length;i++) {
			cedata[i+20] = chName[i]; //characters Name
		}
		
		for(int i=0;i<16;i++) {
			cedata[37+i] = (byte)0x30; //character packets have 16 times 30(0 in ASCII) in row. Mysteries of CRS.
		}
		
		if(this.characterClass == 2) {
			cedata[60] = (byte)0x02; //gender byte
			cedata[68] = (byte)0x02; //class byte
		} else {
			cedata[60] = (byte)0x01; //gender byte
			cedata[68] = (byte)this.characterClass; //class byte
		}
		
		cedata[54] = (byte)0x02; //faction
		cedata[62] = (byte)0x04; //face value
		

		return cedata;
	}
	
	public byte[] getVanishByID(int uid) {
		byte[] vanish = new byte[20];
		byte[] bUid = BitTools.intToByteArray(uid);
		byte[] stuffz = new byte[] {(byte)0x01, (byte)0x10, (byte)0xa0, (byte)0x36, (byte)0x00, (byte)0xee, (byte)0x5f, (byte)0xbf};
		//8 -> (byte)0x01, (byte)0x10, (byte)0xa0, (byte)0x36, 	 16->	(byte)0x00, (byte)0xee, (byte)0x5f, (byte)0xbf
		
		
		vanish[0] = (byte)vanish.length;
		vanish[4] = (byte)0x05;
		
		
		for(int i=0;i<4;i++) { 
			vanish[12+i] = bUid[i];
			vanish[i+8] = stuffz[i];
			vanish[i+16] = stuffz[i+4];
		}
		
		return vanish;
	}

	public Waypoint getLocation() {
		return this.location;
	}
	
}
