package Mob;

import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import ServerCore.ServerFacade;
import World.Area;
import World.Grid;
import World.Location;
import World.WMap;
import World.Waypoint;


/*
 *  Mob.class
 *  Provides basic mob logic functions 
 */
public class Mob implements Location{
        
        
	private int mobID, uid;
	private MobData data;
	private MobController control;
	private List<Integer> iniPackets = new ArrayList<Integer>();
	private List<Integer> needIni = Collections.synchronizedList(new ArrayList<Integer>());
	private ServerFacade sface;
	private Waypoint spawn;
	private Waypoint location;
	int hp,  aggroID, currentWaypoint;
	private boolean alive, aggro;
	private Area area;
	private Grid grid;
	private long died;
	private Map<Integer, Integer> damage = Collections.synchronizedMap(new HashMap<Integer, Integer>());
	private Map<Integer, Waypoint> Path = Collections.synchronizedMap(new HashMap<Integer, Waypoint>());
	private WMap wmap = WMap.getInstance();
        
	/*
	 * Initializes the mob
	 * Params:
	 * mobID = type of mob in question
	 * id = unique ID of mob
	 * mdata = pointer to mobs data object 
	 * * cont = pointer to this mobs MobController object
	 */
        
	public Mob(int mobID, int id, Waypoint mdata, MobController cont) {
		super();
		this.uid = id;
		this.mobID = mobID;
		this.spawn = mdata;
		this.location = new Waypoint(mdata.getX(), mdata.getY());
		this.sface = ServerFacade.getInstance();
		if(this.sface == null) {
			System.out.println("Null facade");
		}
		this.data = cont.getData();
		this.alive = true;
		this.createPath(this.data.getWaypointCount() , this.data.getWaypointHop(), spawn.getX(), spawn.getY());
		this.setCurentWaypoint(0);
		this.control = cont;
		this.wmap.AddMob(id, this);
	}
        
	public int getMobID() {
		return mobID;
	}
	@Override
	public int getuid() {
		return this.uid;
	}
	@Override
	public void setuid(int uid) {
		this.uid = uid;
	}
	@Override
	public float getlastknownX() {
		return this.location.getX();
	}
	@Override
	public float getlastknownY() {
		return this.location.getY();
	}
	@Override
	public SocketChannel GetChannel() {
		return null;
	}
	@Override
    public short getState() {
		return 0;
	}
	// Join mob into the grid based proximity system 
	private void joinGrid(int grid){
		this.reset(false);
		this.grid = this.wmap.getGrid(grid);
		if (!this.hasGrid()){ System.out.println("FUUUUUUUUUuuuuuuuuuuu!!!!!"); }
		else {
			Area a = this.grid.update(this);
			// System.out.println("Got area " + a.getuid());
			this.setMyArea(a);
			this.addAreaMember(this);
		}
	}
	private boolean hasGrid() {
	
		return (this.grid != null);
	}

	private void addAreaMember(Location mob) {
		this.area.addMember(mob);
	}

	private void setMyArea(Area a) {
		this.area = a;
		
	}

	// update our area
	private void updateArea() {
		Area a = this.grid.update(this);
		if (this.getMyArea() != a){
			this.getMyArea().moveTo(this, a);
			this.setMyArea(a);
			a.addMember(this);
		}
	}
	private Area getMyArea() {
		return this.area;
	}

	// remove mob from its current area
	private void rmAreaMember() {
		this.area.rmMember(this);
	}
	// perform actions needed to spawn the mob in the game world
	private void spawnage() {
		this.send(MobPackets.getInitialPacket(this.mobID, this.uid, this.getLocation(), this.hp));
	}
	
	// return total ammount of waypoints generated for the mob
	private int waypointCount()
	{
	    return this.Path.size();
	}
	/* generates the waypoint needed for the idle moving pattern
	 * Params:
	 * wpc = waypoints per each side
	 * hop = distance between waypoints
	 * x = starting x coordinate
	 * y = starting y coordinate
	 */
	private void createPath(int wpc, float hop, float x, float y)
	{
	    Random generator = new Random();
	    int rand = generator.nextInt(100);	
		Waypoint pfft;
		int []tmp = new int[]{0,0};
		// int counter = 0;
		int side = wpc;
		for (int a=0; a < wpc; a++)
		{
		  if ((rand % 2) == 0){ tmp[0] += hop;    }
		  else { tmp[0] -= hop; }
		  pfft = new Waypoint(tmp[0], tmp[1] );
		  this.Path.put(a, pfft);
		  // System.out.println("["+a+"] X:" + pfft[0] + " Y:" + pfft[1]);
		}
		for (int b=wpc; b < (wpc+side); b++)
		{
		  if ((rand % 2) == 0){ tmp[1] += hop;    }
		  else { tmp[1] -= hop; }
		  pfft = new Waypoint(tmp[0], tmp[1] );
		  this.Path.put(b, pfft);
		  // System.out.println("["+b+"] X:" + pfft[0] + " Y:" + pfft[1]);
		}
		wpc += side;
		for (int c=wpc; c < (wpc+side); c++)
		{
		  if ((rand % 2) == 0){ tmp[0] -= hop;  }
		  else { tmp[0] += hop;   }
		  pfft = new Waypoint(tmp[0], tmp[1] );
		  this.Path.put(c, pfft);
		  // System.out.println("["+c+"] X:" + pfft[0] + " Y:" + pfft[1]);
		}
		wpc += side;
		for (int d=wpc; d < (wpc+side); d++)
		{
		  if ((rand % 2) == 0){ tmp[1] -= hop; }
		  else { tmp[1] += hop;  }
		  pfft = new Waypoint(tmp[0], tmp[1] );
		  this.Path.put(d, pfft);
		  // System.out.println("["+d+"] X:" + pfft[0] + " Y:" + pfft[1]);
		}
	  }
	// perform 1 action on the map based on the status of mob
	// return true if players are near, false otherwise
	protected boolean run() {
		if (!this.hasGrid()){ 
			System.out.print("Mob " + this.uid + " Joining grid..");
			this.joinGrid(this.data.getGridID());
			System.out.println("done");
		}
		if (!this.needIni.isEmpty()) this.checkEnvironment();
		
		boolean hasPlayers = false;
		int i = this.getCurentWaypoint();
		Waypoint wp;
		
		// check if mob is alive
		if (this.isAlive()){
			// System.out.println(this.uid + " is alive");
			// check if mob has gone too far from its initial spawn point
			if (WMap.distance(this.location.getX(), this.location.getY(), this.getSpawnx(), this.getSpawny()) > this.data.getMoveRange()){
				System.out.println(this.uid + " is too far from spawn");
				this.reset(true);
			}
			// logic if mob has been aggroed
			else if(this.isAggro()){
				System.out.println(this.uid + " is aggroed by " + this.getAggroID());
				if (this.wmap.CharacterExists(this.getAggroID())){
						Location loc = this.wmap.getCharacter(this.getAggroID());
						// attack target and/or move towards it
						if (WMap.distance(this.location.getX(), this.location.getY(), loc.getlastknownX(), loc.getlastknownY()) < this.data.getAttackRange()){
							// System.out.println(this.uid + " is attacking " + loc.getuid());
							this.attack(loc);
						}
						this.setLocation(loc.getLocation());
						hasPlayers = true;
				}
				else {
					this.reset(true);
				}	
			}
			// mob hasn't been aggroed
			else {
				// move mob to it's next waypoint
				hasPlayers = this.checkAggro();
				if (hasPlayers){
					if (i >= this.waypointCount()) i=0;
				   
					wp = this.getWaypoint(i);
					System.out.println(this.uid + " is moving to x:" + wp.getX() + " y: " + wp.getY());
					this.setLocation(wp);
					i++;
					this.setCurentWaypoint(i);
				}
				
			}
			
		}
		// check if its time to respawn
		else if (System.currentTimeMillis() - this.getDied() > this.data.getRespawnTime()){
			this.setAlive(true);
			this.spawnage();
			
		}
		return hasPlayers;
		
	}
	// attack target loc
	private void attack(Location loc) {
		
	}
	// resets mobs data
	private void reset(boolean sendMove) {
		this.resetDamage();
		this.setHp(this.data.getMaxhp());
		this.setCurentWaypoint(0);
		this.resetToSpawn();
		if (sendMove) this.send(MobPackets.getMovePacket(this.uid, this.location.getX(), this.location.getY()));
		this.setAggro(false);
		this.setAggroID(0);
                
	}
	private void setHp(int maxhp) {
		this.hp = maxhp;
		
	}

	private void resetToSpawn() {
		this.setX(this.getSpawnx());
		this.setY(this.getSpawny());
	}

	// handle damages receiving
	public void recDamge(int uid, int dmg){
		if (this.hasPlayerDamage(uid)){
			int tmp = this.getPlayerDamage(uid);
			tmp += dmg;
			this.setDamage(uid, tmp);
		}
		else{
			this.setDamage(uid, dmg);
		}
                
		Map <Integer, Integer>mp = this.damage;
		synchronized(mp) {  //synchronized iteration for thread safe operations
			Iterator <Map.Entry<Integer, Integer>> it = mp.entrySet().iterator();
			int key;
			int value = 0;
			int hiDmg = 0;
			int hiID = 0;
			while (it.hasNext()) {
				Map.Entry<Integer, Integer> pairs = it.next();
				key = pairs.getKey();
				value = pairs.getValue();
				if (value > hiDmg){
					hiDmg = value;
					hiID = key;
				}
			}
			this.setAggro(true);
			this.setAggroID(hiID);
		}               

		this.reduceHp(dmg);
		if (this.hp <= 0) this.die();
	}
	// perform actions needed to finalize mob's death
	private void die(){
		this.rmAreaMember();
		this.setDied(System.currentTimeMillis());
		this.setAlive(false);
		this.send(MobPackets.getDeathPacket());
		this.reset(false);
	}
	// check if mob is close enough to player to aggro it
	private boolean checkAggro() {
		System.out.println("Mob" + this.uid +" aggrocheck"  + Thread.currentThread());
		boolean hasPlayers = false;
        Map<Integer, Location> mp = new HashMap<Integer, Location>();
        Location loc;      
        mp.putAll(this.area.nearMap());
        synchronized(mp){
          
        	Iterator<Map.Entry<Integer, Location>> iter = mp.entrySet().iterator();
        	while(iter.hasNext()) {
       			Map.Entry<Integer, Location> pairs = iter.next();
       			loc = pairs.getValue();
       			if (loc.GetChannel() != null){
       				hasPlayers = true;
       				if (WMap.distance(this.location.getX(), this.location.getY(), loc.getlastknownX(), loc.getlastknownY()) < this.data.getAggroRange()){
       					this.setAggro(true);
       					this.setAggroID(loc.getuid());
       					break;
       				}
                                         
       			}
       		}
        }
        return hasPlayers;
    }

	// send packet to all nearby players
	private void send(ByteBuffer buf) {		
		this.getMyArea().sendToMembers(this.getuid(), buf);
	}
	// set mobs location on the map and send move packet to players
	private void setLocation(Waypoint wp) {
		this.setX(wp.getX());
		this.setY(wp.getY());
		this.updateArea();
		System.out.println("Mob" + this.uid + "moving to coords x:" + this.location.getX() + ", Y: " + this.location.getY());
		ByteBuffer buf = MobPackets.getMovePacket(this.uid, this.location.getX(), this.location.getY());
		this.send(buf);
	}
	// return reference to this mob's controller
	public MobController getControl() {
		return control;
	}
	private ByteBuffer getInitPacket(){
		return MobPackets.getInitialPacket(this.mobID, this.uid, this.getLocation(), this.hp);
	}
	// update near by objects, called by area
	public void updateEnvironment(List<Integer> players){
		this.needIni.addAll(players);
	}
	// check if mob needs to send initial packet to players
	public void checkEnvironment() {
		Iterator<Integer> plIter = this.iniPackets.iterator();
		Integer tmp = null;
		while(plIter.hasNext()) {
			tmp = plIter.next();
			
			if (!this.wmap.CharacterExists(tmp)){
				this.needIni.remove(tmp);
			} else if(!this.needIni.contains(tmp)) { //if character is no longer in range
				plIter.remove(); //remove it
			} else{
				this.needIni.remove(tmp);
			}
		}
		this.sendInit();
	}
	// send initial packet to everyone in needIni list
	private void sendInit() {
		synchronized(this.needIni){
			Iterator<Integer> siter = this.needIni.iterator();
			Integer tmp = null;
			while(siter.hasNext()) {
				tmp = siter.next();
				if (this.wmap.CharacterExists(tmp)){
					SocketChannel sc =this.wmap.getCharacter(tmp).GetChannel();
					//this.log.severe(this, "init for " + tmp);
					ByteBuffer buf = this.getInitPacket();
					//this.log.severe(this, "got packet");
					// prevent the mob from crashing because there aren't initial packet yet
					if (sc != null) {
						this.sface.addWriteByChannel(sc, buf);
					}
				}
				else {
					siter.remove();
				}
			}
			this.iniPackets.addAll(this.needIni);
			this.needIni.clear();
		}
	}
	private boolean isAlive() {
		return alive;
	}
	private void setAlive(boolean alive) {
		this.alive = alive;
	}
	private void setX(float x) {
		this.location.setX(x);
	}
	private void setY(float y) {
		this.location.setY(y);
	}
	private float getSpawnx() {
		return spawn.getX();
	}
	private float getSpawny() {
		return spawn.getY();
	}
	private void setDamage(int uid, int dmg) {
		this.damage.put(uid, dmg);
	}
	private void resetDamage(){
		this.damage.clear();
	}
	private boolean hasPlayerDamage(int uid){
		return this.damage.containsKey(uid);
	}
	private Waypoint getWaypoint(int i) {
		return this.Path.get(i);
	}
	private boolean isAggro() {
		return aggro;
	}
	private void setAggro(boolean aggro) {
		this.aggro = aggro;
	}
	private int getAggroID() {
		return aggroID;
	}
	private void setAggroID(int aggroID) {
		this.aggroID = aggroID;
	}
	private long getDied() {
		return died;
	}
	private void setDied(long died) {
		this.died = died;
	}
	private int getPlayerDamage(int uid) {
		return this.damage.get(uid);
	}
	private void reduceHp(int dmg) {
		this.hp -= dmg;
	}
	private int getCurentWaypoint() {
		return currentWaypoint;
	}
	private void setCurentWaypoint(int curentWaypoint) {
		this.currentWaypoint = curentWaypoint;
	}

	public Waypoint getLocation() {
		return this.location;
	}
	
        
}
