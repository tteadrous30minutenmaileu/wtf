package Queue;

import java.io.IOException;
import java.nio.BufferOverflowException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingQueue;

import ServerCore.ServerFacade;

import Connections.Connection;
import Connections.ConnectionCollector;
import logging.ServerLogger;



public class WriteHandler implements Runnable {
	private ConnectionCollector con;
	private SocketChannel cn;
	private CountDownLatch cdl;
	private ServerLogger logging = ServerLogger.getInstance();
	private ServerFacade sf = ServerFacade.getInstance();
	
	public WriteHandler(SocketChannel sc, CountDownLatch count) {
		this.cn = sc;
		this.con = this.sf.getConnectionCollector();
		this.cdl = count;
	}
	
	public void run() {
		Connection client = this.con.getConnection(this.cn);
		LinkedBlockingQueue<ByteBuffer> writeBuffer = client.getWriteBuffer();
		SelectionKey skey = this.cn.keyFor(client.getRegisteredSelectors().getWriteSelector());
		boolean wroteAll = true;
		synchronized(writeBuffer) {
			if(writeBuffer.size() > 1) {
				this.packetMerger(writeBuffer);
			}
			while(!writeBuffer.isEmpty()) {
				ByteBuffer tmp = writeBuffer.peek();
				int wrote = 0;
				try {
					if(skey.isWritable()) {
						wrote = this.cn.write(tmp);
					} else {
						wroteAll = false;
						break;
					}
					if(tmp.limit() > 0 && wrote < tmp.limit()) {
						if(wrote == 0) {
							tmp.rewind();
						} else {
							tmp.compact();
						}
						wroteAll = false;
						client.getRegisteredSelectors().getWriteSelector().wakeup();
						break;
					}
						writeBuffer.remove();
					} catch (IOException e) {
						this.disconnect();
						this.logging.warning(this, "Error while writing socket!");
						break;
					} catch(BufferOverflowException be) {
						this.logging.warning(this, "Buffer overflow while writing to: " + client.getIp() + " buffer: " + tmp);
						writeBuffer.remove();
					}
			}
		}
		if(wroteAll) {
			this.sf.flipWriteSelectorOpsByChannel(this.cn, SelectionKey.OP_READ);
		}
		this.cdl.countDown(); //countdown decrement
	}
	
	private void disconnect() {
		this.cdl.countDown();
		Connection con = this.con.getConnection(this.cn);
		if(con != null && con.isRegistered() && con.getRegisteredSelectors() != null) {
			con.threadSafeDisconnect();
		} else {
			this.sf.removeConnection(this.cn);
			try {
				this.cn.close();
			} catch (IOException e) {
				this.logging.warning(this, "Error while closing socket: " + this.cn);
			}
		}
	}
	
	/*
	 * The original MH server would send multiple packets up to 1452 byte chunks
	 * Explicitly merge multiple packets as one to get more packets out at once 
	 */
	private void packetMerger(LinkedBlockingQueue<ByteBuffer> qq) {
		Queue<ByteBuffer> meh = new LinkedList<ByteBuffer>();
		while(!qq.isEmpty()) {
			ByteBuffer ffub = qq.poll();
			if(!qq.isEmpty() && (ffub.limit() + qq.peek().limit()) < 1452) {
				ByteBuffer buff = ByteBuffer.allocateDirect(1452);
				buff.put(ffub);
				while(!qq.isEmpty()) {
					if(qq.peek().limit() <= buff.remaining()) {
						buff.put(qq.poll());
					} else {
						break;
					}
				}
				buff.flip();
				meh.offer(buff);
			} else {
				meh.offer(ffub);
			}
		}
		qq.addAll(meh);
	}
}

