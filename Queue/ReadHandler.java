package Queue;

import java.io.IOException;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingQueue;

import Connections.Connection;
import Connections.ConnectionCollector;
import PacketHandler.PacketHandler;
import ServerCore.ServerFacade;
import logging.ServerLogger;




public class ReadHandler implements Runnable {
	private ConnectionCollector con;
	private SocketChannel cn;
	private PacketHandler han;
	private CountDownLatch cdl;
	private ServerLogger logging = ServerLogger.getInstance();
	private ServerFacade sf = ServerFacade.getInstance();
	private ByteBuffer readBuffer;
	
	public ReadHandler(SocketChannel sc, CountDownLatch count) {
		this.cn = sc;
		this.cdl = count;
		this.han = this.sf.getPckth();
		this.con = this.sf.getConnectionCollector();
		try {
			this.readBuffer = ByteBuffer.allocateDirect(this.cn.socket().getReceiveBufferSize());
		} catch (SocketException e) {
			this.logging.severe(this, "Failed to allocate read buffer for read operation");
		}
	}
	
	public void run() {
		if(this.cn.isOpen() && this.cn.isRegistered()) {
			LinkedBlockingQueue<ByteBuffer> readQueue = this.sf.getConnectionReadQueue(this.cn);
			int read = 0;
			try {
				read = this.cn.read(this.readBuffer);
			} catch (IOException e) {
				this.logging.warning(this, "Error while reading socket");
				this.disconnect();
			}
			if(read > 0) {
				this.readBuffer.flip();
				readQueue.offer(this.readBuffer);
				this.han.processList(this.sf.getConnectionByChannel(this.cn));
			} else if(read == -1) {
				this.disconnect();
			}
			if(read != -1) {
				this.cdl.countDown();
			}
		}
	}
	
	private void disconnect() {
		this.cdl.countDown();
		Connection con = this.con.getConnection(this.cn);
		if(con != null && con.isRegistered() && con.getRegisteredSelectors() != null) {
			con.threadSafeDisconnect();
		} else {
			this.sf.removeConnection(this.cn);
			try {
				this.cn.close();
			} catch (IOException e) {
				this.logging.warning(this, "Error while closing socket: " + this.cn);
			}
		}
	}
}
