package Connections;

import java.io.IOException;
import java.nio.ByteBuffer; 
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.concurrent.LinkedBlockingQueue;

import Queue.PacketQueue;
import ServerCore.SelectorPair;
import ServerCore.ServerFacade;


public class Connection {
	private SocketChannel chan;
	private String ip;
	private PacketQueue pcktq;
	private volatile SelectorPair sepair;
	
	public Connection(SocketChannel sc, int rsize, int wsize) {
		this.chan = sc;
		this.ip = sc.socket().getInetAddress().getHostAddress();
		this.pcktq = new PacketQueue(rsize, wsize);
	}
	
	public Connection(SocketChannel sc, int rsize, int wsize, SelectorPair sp) {
		this.chan = sc;
		this.ip = sc.socket().getInetAddress().getHostAddress();
		this.pcktq = new PacketQueue(rsize, wsize);
		this.sepair = sp;
	}

	public SocketChannel getChan() {
		return chan;
	}

	public String getIp() {
		return ip;
	}
	
	public LinkedBlockingQueue<ByteBuffer> getReadBuffer() {
		return this.pcktq.getReadBuffer();
	}
	
	public LinkedBlockingQueue<ByteBuffer> getWriteBuffer() {
		return this.pcktq.getWriteBuffer();
	}
	
	public void addRead(ByteBuffer bboss) {
		this.pcktq.getReadBuffer().offer(bboss);
	}
	
	public synchronized void registerSelectors(SelectorPair select) {
		this.sepair = select;
		select.registerChannel(this.chan);
	}
	
	public synchronized SelectorPair getRegisteredSelectors() {
		return this.sepair;
	}
	
	public void flipRead(int ops) {
		if(this.isRegistered()) {
			this.sepair.flipRead(this.chan, ops);
		}
	}
	
	public void flipWrite(int ops) {
		if(this.isRegistered()) {
			this.sepair.flipWrite(this.chan, ops);
		}
	}
	
	public synchronized void unregister() {
		if(this.isRegistered()) {
			this.sepair.unregisterChannel(this.chan);
			this.sepair = null;
		}
	}
		
	public boolean disconnect() {
		try {
			this.chan.close();
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public boolean isRegistered() {
		if(this.sepair != null && this.chan.isRegistered()) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public void addWrite(ByteBuffer bboss) {
		if(this.chan.isRegistered()) {
			this.pcktq.getWriteBuffer().offer(bboss);
			this.flipWrite(SelectionKey.OP_WRITE);
		}
	}
	
	public void addWriteButDoNotFlipInterestOps(ByteBuffer bboss) {
		if(this.chan.isRegistered()) {
			this.pcktq.getWriteBuffer().offer(bboss);
		}
	}
	
	public synchronized void setSelectorPair(SelectorPair sp) {
		this.sepair = sp;
	}
	
	public synchronized void threadSafeDisconnect() {
		if(this.sepair == null) {
			try {
				ServerFacade.getInstance().removeConnection(this.chan);
				this.chan.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			this.sepair.registerForFinalization(this);
		}
	}
}
